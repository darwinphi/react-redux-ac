import React, { Component } from 'react'

class Calculator extends Component {
	constructor() {
		super()

		this.state = {
			result: 0,
			operation: 'add'
		}

		this.onCalculate = this.onCalculate.bind(this)
		this.selectOperation = this.selectOperation.bind(this)
		this.firstNumber = React.createRef()
		this.secondNumber = React.createRef()
	}

	selectOperation(event) {
		const operation = event.target.value
		console.log(operation)
		this.setState({ operation })
	}

	onCalculate(event) {
		event.preventDefault()
		const firstNumber = this.firstNumber.current.value
		const secondNumber = this.secondNumber.current.value
		const operation = this.state.operation

		if (firstNumber == '' || secondNumber == '') {
			alert('Please fill up all the inputs')
		} else {

			switch (operation) {
				case 'add':
					var result = parseInt(firstNumber) + parseInt(secondNumber)
					this.setState({result})
					break

				case 'subtract':
					var result = parseInt(firstNumber) - parseInt(secondNumber)
					this.setState({result})
					break

				case 'multiply':
					var result = parseInt(firstNumber) * parseInt(secondNumber)
					this.setState({result})
					break

				case 'divide':
					var result = parseInt(firstNumber) / parseInt(secondNumber)
					this.setState({result})
					break
			}
		}
	}

	render() {

		return (
			<div className="row">
				<div className="col-md-6">
					<form onSubmit={this.onCalculate}>
						<div className="form-group row">
							<label className="col-sm-3 col-form-label">1st Number</label>
							<div className="col-sm-9">
								<input 	type="number"
												className="form-control"
												ref={this.firstNumber}
								/>
							</div>
						</div>
						<div className="form-group row">
							<label className="col-sm-3 col-form-label">2nd Number</label>
							<div className="col-sm-9">
								<input 	type="number"
												className="form-control"
												ref={this.secondNumber}
								/>
							</div>
						</div>

						<div className="form-group">
							<div className="form-check form-check-inline">
							  <input 	className="form-check-input"
							  				type="radio"
							  				name="radioButton"
							  				id="add"
							  				onChange={this.selectOperation}
							  				value="add"
							  				
							  />
							  <label className="form-check-label" htmlFor="add">Add</label>
							</div>
							<div className="form-check form-check-inline">
							  <input 	className="form-check-input"
							  				type="radio"
							  				name="radioButton"
							  				id="subtract"
							  				onChange={this.selectOperation}
							  				value="subtract"
							  />
							  <label className="form-check-label" htmlFor="subtract">Subtract</label>
							</div>
						</div>

						<div className="input-group mb-3 float-left">
						  <div className="input-group-prepend">
						    <label className="input-group-text">Operation</label>
						  </div>
						  <select className="custom-select" onChange={this.selectOperation}>
						    <option value="add">Add</option>
						    <option value="subtract">Subtract</option>
						    <option value="multiply">Multiply</option>
						    <option value="divide">Divide</option>
						  </select>
						</div>
						<button type="submit" className="btn btn-primary float-right">Calculate</button>
					</form>

					<h2>Result: {this.state.result}</h2>
					<hr/>
				</div>
			</div>
		)
	}
}

export default Calculator