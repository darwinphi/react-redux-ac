import React, { Component } from 'react'

class Main extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<div>
				<button className="btn btn-primary" onClick={this.props.changeName}>Set Name</button>
			</div>
		)
	}
} 

export default Main