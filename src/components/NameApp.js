import React, { Component } from 'react'

class NameApp extends Component {
	constructor(props) {
		super(props)

		this.state = {
			name: '',
			fullName: ''
		}

		this.setName = this.setName.bind(this)
		this.changeName = this.changeName.bind(this)

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	setName(age) {
		alert(`Hi ${this.state.name}, you are ${age} yrs. old`)
	}

	changeName(event) {
		this.setState({
			name: event.target.value
		})
	}

	handleChange(event) {
		this.setState({
			fullName: event.target.value
		})
	}

	handleSubmit(event) {
		alert(`Your fullname is ${this.state.fullName}`)
		event.preventDefault()
	}

	render() {
		return (
			<div className="row">
				<div className="col-md-6">
					<div className="form-group">
						<label>Full Name:</label>
						<input type="text" onChange={this.changeName} className="form-control"/>
					</div>
					<button type="submit"
									className="btn btn-primary"
									onClick={() => this.setName(22)}
					>
									
									Submit
					</button>

					{ this.state.name }
				</div>
				<div className="col-md-6">
					<form onSubmit={this.handleSubmit}>
						<div className="form-group">
							<label>Name:</label>
							<input type="text" onChange={this.handleChange} className="form-control"/>
						</div>
						<input type="submit" value="Submit" className="btn btn-primary"/>
					</form>
				</div>
			</div>
		)
	}
}

export default NameApp