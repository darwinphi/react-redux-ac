import React, { Component } from 'react'

class Todo extends Component {
	constructor(props) {
		super(props)

		this.state = {
			users: [],
			isLoading: true
		}


		this.inputName = React.createRef()
		this.onDeleteUser = this.onDeleteUser.bind(this)
		this.addUser = this.addUser.bind(this)
	}

	componentDidMount() {
		fetch('http://jsonplaceholder.typicode.com/users')
			.then(data => data.json())
			.then(users => this.setState({
				users: users,
				isLoading: false
			}))
	}

	onDeleteUser(id) {
		const updatedUsers = this.state.users.filter(user => user.id !== id)
		this.setState({
			users: updatedUsers
		})
	}

	addUser() {
		console.log(this.inputName.current.value)
		const name = this.inputName.current.value
		const id = Math.floor(Math.random() * 1000)
		this.setState({
			users: [...this.state.users, { name, id }]
		})
	}

	render() {
		console.log(this.state.users)
		const displayUsers = this.state.users.map(user => {
			return 	<li key={user.id} className="list-group-item">
							<label>{user.name}</label>
							<button onClick={() => this.onDeleteUser(user.id)} className="btn btn-danger float-right">x</button>
							</li>
		})

		return (
			<div className="row">
				<div className="col-md-6">

					<h1>Add User</h1>
					<form>
						<div className="form-group">
							<input 	type="text"
											className="form-control"
											placeholder="Name"
											ref={this.inputName}
							/>
						</div>
						<button className="btn btn-block btn-primary"
										onClick={() => this.addUser()}>
										Add User
						</button>
					</form>

					<h2>List of Users</h2>
					{ 
						this.state.users && displayUsers
					}
					{
						this.state.isLoading && <label>Loading...</label>
					}
				</div>
			</div>
		)
	}
}

export default Todo