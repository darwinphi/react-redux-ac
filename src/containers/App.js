import React, { Component } from 'react'
import { connect } from 'react-redux'

import Main from '../components/Main'
import User from '../components/User'

import AppNavBar from '../components/AppNavBar'
import NameApp from '../components/NameApp'
import Todo from '../components/Todo'
import Calculator from '../components/Calculator'

import { setName } from '../actions/userActions'

import './bootstrap.min.css'

import { HashRouter, BrowserRouter as Router, Link, Route } from 'react-router-dom'

const About = () => {
	return (
		<div><h1>About Page</h1></div>
	)
}

const Contact = () => {
	return (
		<div><h1>Contact Page</h1></div>
	)
}

const Home = () => {
	return (
		<div><h1>Home Page</h1></div>
	)
}



class App extends Component {
	render() {
		return (
			<HashRouter>
				<div className="container">
					{/*<Todo />*/}
					{/*<AppNavBar/>*/}

					<Calculator />

					<h1>Hello World</h1>
					<Main changeName={() => this.props.setName('John')} />
					<User name={this.props.user.name} />
					
					<li><Link to="/">Home</Link></li>
					<li><Link to="/about">About</Link></li>
					<li><Link to="/contact">Contact</Link></li>

					<Route exact={true} path="/" component={Home}/>
					<Route path="/about" component={About}/>
					<Route path="/contact" component={Contact}/>

					<NameApp />
				</div>
			</HashRouter>
		)
	}
} 

const mapStateToProps = (state) => {
	return {
		user: state.userReducer,
		math: state.mathReducer
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setName: (name) => {
			dispatch(setName(name))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App)